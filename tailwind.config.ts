import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      colors: {
        "vikeo-pink": {
          "50": "#fef2f5",
          "100": "#fde6ed",
          "200": "#fad1de",
          "300": "#f494b3",
          "400": "#f17ba4",
          "500": "#e64d85",
          "600": "#d22c71",
          "700": "#b11f60",
          "800": "#941d56",
          "900": "#7f1c4e",
          "950": "#470a27",
        },
        "vikeo-blue": {
          "50": "#f1f6fd",
          "100": "#deeafb",
          "200": "#c5dbf8",
          "300": "#9dc5f3",
          "400": "#6fa6eb",
          "500": "#4d84e4",
          "600": "#3868d8",
          "700": "#2f55c6",
          "800": "#2c46a1",
          "900": "#2b4288",
          "950": "#1d274e",
        },
      },
    },
  },
  plugins: [],
};
export default config;
