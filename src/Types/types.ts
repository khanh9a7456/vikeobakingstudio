export interface Product {
  id: string;
  name: string;
  price: number;
  image: string;
  categories: Array<string>;
  description: string;
  quantity: number;
}

export interface IShippingInfo {
  name: string;
  phone: string;
  address: string;
  orderType: "order-now" | "pre-order";
  deliveryType: "ship" | "get";
  "receive-time": string;
  products: string;
  paymentMethod: "cod" | "transfer";
  total: string;
}

export interface IAddressItem {
  place_id: number;
  licence: string;
  osm_type: string;
  osm_id: number;
  lat: string;
  lon: string;
  class: string;
  type: string;
  place_rank: number;
  importance: number;
  addresstype: string;
  name: string;
  display_name: string;
  boundingbox: string[];
}
