import { IAddressItem } from "@/Types/types";
import { https } from "./configURL";

export const addressService = {
  getAddressData: async (query: string): Promise<IAddressItem[]> => {
    let response = await https.get(
      `https://nominatim.openstreetmap.org/search?format=json&q=${query}&countrycodes=VN`,
    );
    return response.data;
  },
  getGrabAddressData: async (query: string): Promise<IAddressItem[]> => {
    let response = await https.get(
      `https://food.grab.com/proxy/foodweb/v1/order/geo/search-poi?countryCode=VN`,
    );
    return response.data;
  },
};
