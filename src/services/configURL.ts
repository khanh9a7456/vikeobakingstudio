import axios from "axios";

export const https = axios.create({
  baseURL: "https://api.themoviedb.org",
  params: {
    region: "US",
    sort: "popularity",
    sources: "netflix,hulu",
    offset: "0",
    limit: "5",
  },
  headers: {
    accept: "application/json",
    Authorization:
      "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI3MDQ0OWYwMTM3ZDljYTA4NTA2NTljMzUwYmI3ZWM5YyIsInN1YiI6IjY0N2M5ZDNmY2Y0YjhiMDBhODc4MTY4OSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.r8KkJvcmV7NgMVko3GFb0aYno9YHNdbnY4yx_-Km11E",
  },
});
