"use client";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "@/lib/store";
import {
  clearCart,
  removeFromCart,
  updateQuantity,
} from "@/lib/features/cart/cartSlice";
import Image from "next/image";
import { useRouter } from "next/navigation";
import { useState } from "react";
import { FiTrash2 } from "react-icons/fi";
import Link from "next/link";
import { IShippingInfo } from "@/Types/types";
import { postCheckoutData } from "@/hooks/usePostCheckoutData";
import { formatDate } from "@/utils/formatDate";
import { formatDateTimeLocal } from "@/utils/formatDateTimeLocal";
import { QRCodeComponent } from "../QRCode";
import { useGetAddressListByQuery } from "@/hooks/useGetAddressListByQuery";
import ComboBox from "./ComboBox";
import useDebounce from "@/hooks/useDebounce";

const CartAndCheckoutPage: React.FC = () => {
  const cart = useSelector((state: RootState) => state.cart);
  const dispatch = useDispatch();
  const router = useRouter();
  const today = new Date();
  const totalAmount = cart.products.reduce(
    (total, product) => total + product.price * product.quantity,
    0,
  );

  const cartString = cart.products
    ?.map((item, index) => {
      return `<p>${index + 1}. ${item.name}:</p><p>- Số lượng: ${item.quantity}</p><p>- Giá: ${(
        item.price * item.quantity
      ).toLocaleString("vi-VN", {
        style: "currency",
        currency: "VND",
        maximumFractionDigits: 0,
      })}</p>`;
    })
    .join("");

  const [shippingInfo, setShippingInfo] = useState<IShippingInfo>({
    name: "",
    address: "",
    phone: "",
    orderType: "order-now",
    deliveryType: "ship",
    "receive-time": formatDate(today),
    products: cartString,
    paymentMethod: "cod",
    total: totalAmount.toLocaleString("vi-VN", {
      style: "currency",
      currency: "VND",
      maximumFractionDigits: 0,
    }),
  });

  const debouncedInputValue = useDebounce<string>(shippingInfo.address, 500);
  const { data } = useGetAddressListByQuery(debouncedInputValue);
  const addressListFromApi = data?.map((address) => ({
    id: address.place_id,
    name: address.display_name,
  }));

  const handleQuantityChange = (id: string, quantity: number) => {
    dispatch(updateQuantity({ id, quantity }));
  };

  const handleRemove = (id: string) => {
    dispatch(removeFromCart(id));
  };

  const handleClearCart = () => {
    dispatch(clearCart());
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.name === "receive-time") {
      setShippingInfo({
        ...shippingInfo,
        ["receive-time"]: formatDate(new Date(e.target.value)),
      });
    } else {
      setShippingInfo({ ...shippingInfo, [e.target.name]: e.target.value });
    }
  };

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    // Handle checkout process here (e.g., save shipping info, redirect to payment page)
    postCheckoutData(shippingInfo);
    router.push("/confirmation");
    handleClearCart();
  };

  return (
    <main className="w-full px-4 py-8">
      <h1 className="mb-8 text-3xl font-bold">Giỏ Hàng và Thanh Toán</h1>
      <div className="flex w-full flex-col justify-between gap-10 md:flex-row">
        <div className="flex-[2]">
          <h2 className="mb-2 text-2xl font-bold">Giỏ Hàng</h2>
          {cart.products.length === 0 ? (
            <p className="italic">Vui lòng chọn sản phẩm.</p>
          ) : (
            <div>
              <ul>
                {cart.products.map((product) => (
                  <li
                    key={product.id}
                    className="mb-4 flex items-center justify-between"
                  >
                    <div className="flex items-center">
                      <Link href={`/products/${product.id}`}>
                        <Image
                          src={product.image}
                          alt={product.name}
                          className="mr-4 h-20 w-20 rounded-md object-cover"
                          width={50000}
                          height={50000}
                        />
                      </Link>
                      <div>
                        <h3 className="text-xl font-bold">{product.name}</h3>
                        <p
                          style={{ fontWeight: 700, fontSize: "20px" }}
                          className="font-semibold text-gray-600"
                        >
                          {product.price.toLocaleString("vi-VN", {
                            style: "currency",
                            currency: "VND",
                            maximumFractionDigits: 0,
                          })}
                        </p>
                        <input
                          type="number"
                          min="1"
                          value={product.quantity}
                          onChange={(e) =>
                            handleQuantityChange(
                              product.id,
                              parseInt(e.target.value),
                            )
                          }
                          className="mt-2 w-16 rounded-md border border-gray-300 p-2"
                        />
                      </div>
                    </div>
                    <button
                      onClick={() => handleRemove(product.id)}
                      className="rounded-md bg-red-600 p-2 text-white transition hover:bg-red-700"
                    >
                      <FiTrash2 className="text-xl" />
                    </button>
                  </li>
                ))}
              </ul>
              <div className="mt-8 flex items-center justify-between">
                <button
                  onClick={handleClearCart}
                  className="rounded-md bg-red-600 px-4 py-2 text-white transition hover:bg-red-700"
                >
                  Clear Cart
                </button>
                <div className="text-xl font-bold">
                  Total:{" "}
                  {totalAmount.toLocaleString("vi-VN", {
                    style: "currency",
                    currency: "VND",
                    maximumFractionDigits: 0,
                  })}
                </div>
              </div>
            </div>
          )}
        </div>
        <div className="flex-1 text-vikeo-blue-900">
          <h2 className="text-2xl font-bold">Thông Tin Khách Hàng</h2>
          <form
            onSubmit={handleSubmit}
            className="mx-auto mt-4 max-w-lg rounded-md border bg-white p-5 shadow-sm"
          >
            <div className="mb-4">
              <label htmlFor="name" className="block font-semibold">
                Tên khách hàng
              </label>
              <input
                type="text"
                id="name"
                name="name"
                value={shippingInfo.name}
                onChange={handleChange}
                required
                className="mt-1 block w-full rounded-md border border-gray-300 p-2"
              />
            </div>

            <div className="mb-4">
              <label htmlFor="phone" className="block font-semibold">
                Số điện thoại
              </label>
              <input
                type="tel"
                id="phone"
                name="phone"
                value={shippingInfo.phone}
                onChange={handleChange}
                required
                className="mt-1 block w-full rounded-md border border-gray-300 p-2"
              />
            </div>

            <div className="mb-4">
              <label htmlFor="address" className="block font-semibold">
                Địa chỉ
              </label>
              <ComboBox
                handleChange={handleChange}
                addressListFromApi={addressListFromApi ?? []}
                setShippingInfo={setShippingInfo}
                shippingInfo={shippingInfo}
              />
            </div>

            <div className="mb-4">
              <p className="block font-semibold">Nhận hàng</p>
              <div className="mt-3 flex flex-wrap items-center gap-5">
                <div className="flex items-center">
                  <input
                    type="radio"
                    id="ship"
                    name="deliveryType"
                    value="ship"
                    checked={shippingInfo.deliveryType === "ship"}
                    onChange={handleChange}
                    className="mr-2"
                  />
                  <label htmlFor="ship" className="text-sm">
                    Ship (Grab)
                  </label>
                </div>

                <div className="flex items-center">
                  <input
                    type="radio"
                    id="get"
                    name="deliveryType"
                    value="get"
                    checked={shippingInfo.deliveryType === "get"}
                    onChange={handleChange}
                    className="mr-2"
                  />
                  <label htmlFor="get" className="text-sm">
                    Lấy tại shop
                  </label>
                </div>
              </div>
            </div>

            <div className="mb-4">
              <p className="block font-semibold">Đặt hàng</p>
              <div className="mt-3 flex flex-wrap items-center gap-5">
                <div className="flex items-center">
                  <input
                    type="radio"
                    id="order-now"
                    name="orderType"
                    value="order-now"
                    checked={shippingInfo.orderType === "order-now"}
                    onChange={handleChange}
                    className="mr-2"
                  />
                  <label htmlFor="order-now" className="text-sm">
                    Đặt ngay
                  </label>
                </div>
                <div className="flex items-center">
                  <input
                    type="radio"
                    id="pre-order"
                    name="orderType"
                    value="pre-order"
                    checked={shippingInfo.orderType === "pre-order"}
                    onChange={handleChange}
                    className="mr-2"
                  />
                  <label htmlFor="pre-order" className="text-sm">
                    Đặt trước
                  </label>
                </div>
              </div>
            </div>

            {shippingInfo.orderType === "pre-order" ? (
              <div className="mb-4">
                <label htmlFor="receive-time" className="block font-semibold">
                  Thời gian nhận
                </label>
                <input
                  type="datetime-local"
                  id="receive-time"
                  name="receive-time"
                  value={formatDateTimeLocal(today)}
                  onChange={handleChange}
                  required
                  className="mt-1 block w-full rounded-md border border-gray-300 p-2"
                />
              </div>
            ) : (
              <></>
            )}

            <div className="mb-4">
              <p className="block font-semibold">Thanh toán</p>
              <div className="mt-3 flex flex-wrap items-center gap-5">
                <div className="flex items-center">
                  <input
                    type="radio"
                    id="cod"
                    name="paymentMethod"
                    value="cod"
                    checked={shippingInfo.paymentMethod === "cod"}
                    onChange={handleChange}
                    className="mr-2"
                  />
                  <label htmlFor="cod" className="text-sm">
                    COD
                  </label>
                </div>
                <div className="flex items-center">
                  <input
                    type="radio"
                    id="transfer"
                    name="paymentMethod"
                    value="transfer"
                    checked={shippingInfo.paymentMethod === "transfer"}
                    onChange={handleChange}
                    className="mr-2"
                  />
                  <label htmlFor="transfer" className="text-sm">
                    Chuyển khoản
                  </label>
                </div>
              </div>
            </div>

            {shippingInfo.paymentMethod === "transfer" ? (
              <QRCodeComponent />
            ) : (
              <></>
            )}

            {shippingInfo.deliveryType === "ship" ? (
              <p className="my-3 text-sm italic text-vikeo-pink-600">
                Shop sẽ liên hệ với bạn để xác nhận đơn hàng và báo phí ship.
              </p>
            ) : (
              <></>
            )}
            <button
              disabled={cart.products.length === 0}
              type="submit"
              className="w-full rounded-md bg-vikeo-blue-900 px-4 py-2 text-white transition hover:bg-vikeo-blue-600 disabled:cursor-not-allowed disabled:opacity-80 disabled:hover:bg-vikeo-blue-900"
            >
              Hoàn tất
            </button>
          </form>
        </div>
      </div>
    </main>
  );
};

export default CartAndCheckoutPage;
