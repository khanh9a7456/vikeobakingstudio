import { IShippingInfo } from "@/Types/types";
import {
  Combobox,
  ComboboxButton,
  ComboboxInput,
  ComboboxOption,
  ComboboxOptions,
} from "@headlessui/react";
import React, { ChangeEvent, Dispatch, SetStateAction } from "react";
import { FaCheck } from "react-icons/fa6";
import { FiChevronDown } from "react-icons/fi";

interface Props {
  handleChange: (e: ChangeEvent<HTMLInputElement>) => void;
  setShippingInfo: Dispatch<SetStateAction<IShippingInfo>>;
  shippingInfo: IShippingInfo;
  addressListFromApi: {
    id: number;
    name: string;
  }[];
}

function ComboBox({
  setShippingInfo,
  shippingInfo,
  addressListFromApi,
}: Props) {
  const handleComboBox = (value: { id: number; name: string }) => {
    console.log("value: ", value.name);
    console.log("shippingInfo.address: ", shippingInfo.address);
    setShippingInfo({
      ...shippingInfo,
      address:
        shippingInfo.address.length > value?.name?.length
          ? shippingInfo.address
          : value?.name,
    });
  };
  return (
    <div className="w-full">
      <Combobox
        onChange={(value: { id: number; name: string }) =>
          handleComboBox(value)
        }
      >
        <div className="relative">
          <ComboboxInput
            className="mt-1 block w-full rounded-md border border-gray-300 p-2 pr-8"
            displayValue={(address: { id: number; name: string }) =>
              shippingInfo.address.length > address?.name?.length
                ? shippingInfo.address
                : address?.name
            }
            onChange={(event) =>
              setShippingInfo({ ...shippingInfo, address: event.target.value })
            }
          />
          <ComboboxButton className="group absolute inset-y-0 right-0 px-2.5">
            <FiChevronDown className="size-4 fill-white/60 group-data-[hover]:fill-white" />
          </ComboboxButton>
        </div>

        <ComboboxOptions
          anchor="bottom"
          className="w-fit rounded-xl border border-white/5 bg-white p-1 transition duration-100 ease-in [--anchor-gap:var(--spacing-1)] empty:invisible data-[leave]:data-[closed]:opacity-0"
        >
          {addressListFromApi?.map((address) => (
            <ComboboxOption
              key={address.id}
              value={address}
              className="group flex cursor-default select-none items-center gap-2 rounded-lg px-3 py-1.5 data-[focus]:bg-white/10"
            >
              <FaCheck className="invisible size-4 fill-vikeo-blue-900 group-data-[selected]:visible" />
              <div className="text-sm/6 text-vikeo-blue-900">
                {address.name}
              </div>
            </ComboboxOption>
          ))}
        </ComboboxOptions>
      </Combobox>
    </div>
  );
}

export default ComboBox;
