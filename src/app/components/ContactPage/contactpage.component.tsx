"use client";

import React from "react";
import ContactForm from "../Contact/contact.component";

function ContactPage() {
  return (
    <main className="container mx-auto px-4 py-8">
      <ContactForm />
    </main>
  );
}

export default ContactPage;
