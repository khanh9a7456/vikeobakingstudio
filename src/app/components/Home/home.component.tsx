"use client";
import React from "react";
import { Carousel } from "../Carousel";
import { categories, products } from "../product";
import ProductCard from "../Products/ProductCard/productcard.component";

function HomePage() {
  const onRenderProductByCategories = () => {
    return categories
      .filter((cate) => cate !== "All")
      .map((cate) => {
        return (
          <div key={cate}>
            <h2 className="py-5 text-2xl font-semibold">{cate}</h2>
            <div className="grid grid-cols-2 gap-3 sm:grid-cols-3 md:gap-8 lg:grid-cols-4">
              {products
                .filter((p) => p.categories.includes(cate))
                .map((product) => (
                  <React.Fragment key={product.name}>
                    <ProductCard product={product} />
                  </React.Fragment>
                ))}
            </div>
          </div>
        );
      });
  };

  return (
    <main>
      <Carousel />
      <section id="contact" className="py-12">
        <div className="container mx-auto px-4">
          <div className="">{onRenderProductByCategories()}</div>
        </div>
      </section>
    </main>
  );
}

export default HomePage;
