import React, { useState } from "react";
import defaultBanner from "/public/Banner/default.png";
import Image from "next/image";
import { Autoplay, Keyboard, Navigation, Pagination } from "swiper/modules";
import { Swiper, SwiperSlide } from "swiper/react";
// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import "swiper/css/autoplay";

function Carousel() {
  const array = [
    { id: 1, imgSrc: defaultBanner, title: "banner" },
    { id: 2, imgSrc: defaultBanner, title: "banner" },
    { id: 3, imgSrc: defaultBanner, title: "banner" },
  ];
  const onRenderCarousel = () => {
    return array?.map((item) => {
      return (
        <SwiperSlide key={item.id}>
          <div className="relative">
            <Image
              src={item.imgSrc}
              alt={item.title}
              className="h-auto w-screen object-cover saturate-100"
              width={90000000}
              height={90000000}
            />
          </div>
        </SwiperSlide>
      );
    });
  };

  return (
    <Swiper
      modules={[Pagination, Navigation, Keyboard, Autoplay]}
      slidesPerView={1}
      navigation={true}
      keyboard={{ enabled: true, onlyInViewport: true }}
      speed={500}
      style={{ zIndex: "0" }}
      className="brightness-100"
      autoplay
    >
      {onRenderCarousel()}
    </Swiper>
  );
}

export default Carousel;
