"use client";
import { useSelector, useDispatch } from "react-redux";
import Link from "next/link";
import { FiX } from "react-icons/fi";
import { RootState } from "@/lib/store";
import { removeFromCart, updateQuantity } from "@/lib/features/cart/cartSlice";
import Image from "next/image";
import { FiTrash2 } from "react-icons/fi";

interface CartSidebarProps {
  isOpen: boolean;
  toggleCartSidebar: () => void;
}

const CartSidebar: React.FC<CartSidebarProps> = ({
  isOpen,
  toggleCartSidebar,
}) => {
  const cart = useSelector((state: RootState) => state.cart);
  const dispatch = useDispatch();

  const handleQuantityChange = (id: string, quantity: number) => {
    dispatch(updateQuantity({ id, quantity }));
  };

  const handleRemove = (id: string) => {
    dispatch(removeFromCart(id));
  };

  const totalAmount = cart.products.reduce(
    (total, product) => total + product.price * product.quantity,
    0,
  );

  return (
    <div
      className={`fixed right-0 top-0 z-50 h-full w-64 transform bg-white ${
        isOpen ? "translate-x-0" : "translate-x-full"
      } w-full overflow-auto text-vikeo-blue-900 shadow-lg transition-transform duration-300 ease-in-out md:w-1/3`}
    >
      <div className="flex items-center justify-between border-b border-gray-200 p-4">
        <h2 className="text-lg font-bold">Cart</h2>
        <button onClick={toggleCartSidebar} aria-label="Close Cart">
          <FiX className="text-2xl" />
        </button>
      </div>
      <div className="overflow-y-auto p-4">
        {cart.products.length === 0 ? (
          <p>Your cart is empty.</p>
        ) : (
          <ul>
            {cart.products.map((product) => (
              <li
                key={product.id}
                className="mb-4 flex items-center justify-between"
              >
                <div className="flex items-center">
                  <Image
                    src={product.image}
                    alt={product.name}
                    className="mr-4 h-16 w-16 object-cover"
                    width={50000}
                    height={50000}
                  />
                  <div>
                    <h3 className="text-lg font-bold">{product.name}</h3>
                    <p className="text-gray-600">
                      {product.price.toLocaleString("vi-VN", {
                        style: "currency",
                        currency: "VND",
                        maximumFractionDigits: 0,
                      })}
                    </p>
                    <input
                      type="number"
                      min="1"
                      value={product.quantity}
                      onChange={(e) =>
                        handleQuantityChange(
                          product.id,
                          parseInt(e.target.value),
                        )
                      }
                      className="mt-2 w-12 rounded-md border border-gray-300 p-1"
                    />
                  </div>
                </div>
                <button
                  onClick={() => handleRemove(product.id)}
                  className="text-red-600 hover:underline"
                >
                  <FiTrash2 className="text-2xl transition-transform hover:scale-110" />
                </button>
              </li>
            ))}
          </ul>
        )}
      </div>
      <div className="border-t border-gray-200 p-4">
        <div className="mb-4 flex items-center justify-between">
          <span className="text-lg font-bold">Total:</span>
          <span className="text-lg font-bold">
            {totalAmount.toLocaleString("vi-VN", {
              style: "currency",
              currency: "VND",
              maximumFractionDigits: 0,
            })}
          </span>
        </div>
        <Link
          href="/cart-checkout"
          onClick={toggleCartSidebar}
          className="block w-full rounded-md bg-vikeo-blue-900 px-4 py-2 text-center text-white transition hover:bg-vikeo-blue-600"
        >
          Go to Cart
        </Link>
      </div>
    </div>
  );
};

export default CartSidebar;
