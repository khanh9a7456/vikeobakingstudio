"use client";

import Link from "next/link";
import React, { useEffect, useRef, useState } from "react";
import { FiChevronDown, FiMenu, FiShoppingCart, FiX } from "react-icons/fi";
import Logo from "../Logo/logo.component";
import useOnTop from "@/hooks/useOnTop";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { useSelector } from "react-redux";
import { RootState } from "@/lib/store";
import CartSidebar from "./parts/CartSidebar";
import { categories } from "../product";

export default function RootHeader() {
  const [sidebarOpen, setSidebarOpen] = useState(false);
  const [cartSidebarOpen, setCartSidebarOpen] = useState(false);
  const cart = useSelector((state: RootState) => state.cart);
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const dropdownRef = useRef<HTMLDivElement>(null);
  const searchParams = useSearchParams();
  const tabIndex = Number(searchParams.get("tab"));
  const isOnTop = useOnTop();
  const pathName = usePathname();
  const router = useRouter();

  const toggleSidebar = () => {
    setSidebarOpen(!sidebarOpen);
  };

  const toggleCartSidebar = () => {
    setCartSidebarOpen(!cartSidebarOpen);
    setSidebarOpen(false);
  };

  const totalQuantity = cart.products.reduce(
    (total, product) => total + product.quantity,
    0,
  );

  const handleDropdownToggle = () => {
    setDropdownOpen(!dropdownOpen);
  };

  const handleOutsideClick = (event: MouseEvent) => {
    if (
      dropdownRef.current &&
      !dropdownRef.current.contains(event.target as Node)
    ) {
      setDropdownOpen(false);
    }
  };

  useEffect(() => {
    if (dropdownOpen) {
      document.addEventListener("mousedown", handleOutsideClick);
    } else {
      document.removeEventListener("mousedown", handleOutsideClick);
    }
    return () => {
      document.removeEventListener("mousedown", handleOutsideClick);
    };
  }, [dropdownOpen]);

  return (
    <header
      className={`${
        !isOnTop || pathName !== "/"
          ? "bg-vikeo-pink-300 text-white"
          : "text-vikeo-blue-900"
      } ${
        pathName === "/" ? "fixed" : "sticky"
      } left-0 right-0 top-0 z-10 px-4`}
    >
      <div className="container mx-auto flex items-center justify-between">
        <div
          className="cursor-pointer text-2xl md:hidden"
          onClick={toggleSidebar}
          aria-label="Toggle Menu"
        >
          {sidebarOpen ? <FiX /> : <FiMenu />}
        </div>
        <Logo />
        <button
          onClick={toggleCartSidebar}
          className="relative block focus:outline-none md:hidden"
        >
          <FiShoppingCart className="text-2xl" />
          {totalQuantity > 0 && (
            <p className="absolute -right-2 -top-2 flex h-4 w-4 items-center justify-center rounded-full bg-red-600 text-[10px] text-white">
              {totalQuantity}
            </p>
          )}
        </button>
        <nav className="hidden font-medium md:flex md:items-center">
          <Link href="/">
            <button className="mr-4 text-sm uppercase hover:underline">
              Trang Chủ
            </button>
          </Link>
          <div className="group relative">
            <div
              onClick={() => setDropdownOpen(false)}
              className="mr-4 flex translate-y-2 items-center space-x-2 pb-2 hover:underline focus:outline-none"
            >
              <Link href="/products">
                <button className="text-sm uppercase hover:underline">
                  Sản Phẩm
                </button>
              </Link>
              <FiChevronDown />
            </div>
            <div className="pt-2" onMouseOut={() => setDropdownOpen(false)}>
              <div
                className={`absolute left-0 top-full w-48 rounded-md bg-vikeo-blue-900 text-black shadow-lg transition-opacity duration-300 ${
                  dropdownOpen ? "opacity-100" : "pointer-events-none opacity-0"
                } group-hover:pointer-events-auto group-hover:opacity-100`}
              >
                <div className="py-2">
                  {categories.map((cate, index) => {
                    return (
                      <Link
                        key={cate + index}
                        href={`/products?tab=${index}`}
                        className={`${tabIndex === index ? "text-white" : "text-vikeo-blue-200"} block px-4 py-2 hover:text-white hover:underline`}
                        onClick={() => setDropdownOpen(false)}
                      >
                        {cate}
                      </Link>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>

          <Link href="/contact">
            <button className="mr-4 text-sm uppercase hover:underline">
              Liên Hệ
            </button>
          </Link>
          <button
            onClick={toggleCartSidebar}
            className="relative focus:outline-none"
          >
            <FiShoppingCart className="text-2xl" />
            {totalQuantity > 0 && (
              <span className="absolute -right-2 -top-2 flex h-5 w-5 items-center justify-center rounded-full bg-red-600 text-xs text-white">
                {totalQuantity}
              </span>
            )}
          </button>
        </nav>
      </div>
      <div
        className={`fixed left-0 top-0 z-50 h-full w-64 transform bg-vikeo-blue-900 text-white ${
          sidebarOpen ? "-translate-x-0" : "-translate-x-full"
        } transition-transform duration-300 ease-in-out md:hidden`}
      >
        <div className="flex items-center justify-between border-b p-4">
          <div className="text-lg font-bold">Menu</div>
          <button onClick={toggleSidebar} aria-label="Close Menu">
            <FiX className="text-2xl" />
          </button>
        </div>
        <div className="p-4">
          <Link href="/">
            <button
              className="block py-2 text-sm uppercase hover:underline"
              onClick={toggleSidebar}
            >
              Trang Chủ
            </button>
          </Link>
          <div className="relative">
            <button
              onClick={handleDropdownToggle}
              className="flex w-full items-center justify-between py-2 hover:underline focus:outline-none"
            >
              <span
                onClick={() => {
                  router.push("/products");
                  setSidebarOpen(false);
                }}
              >
                Products
              </span>
              <FiChevronDown />
            </button>

            <div
              ref={dropdownRef}
              className={`mt-2 transition-all duration-300 ease-in-out ${
                dropdownOpen ? "max-h-screen opacity-100" : "max-h-0 opacity-0"
              } overflow-hidden`}
            >
              {categories.map((cate, index) => {
                return (
                  <Link
                    key={cate + index}
                    href={`/products?tab=${index}`}
                    className={`${tabIndex === index ? "text-vikeo-blue-400" : "text-vikeo-blue-900"} block px-4 py-2 hover:underline`}
                    onClick={() => setDropdownOpen(false)}
                  >
                    {cate}
                  </Link>
                );
              })}
            </div>
          </div>
          <Link href="/contact">
            <button
              className="block py-2 text-sm uppercase hover:underline"
              onClick={toggleSidebar}
            >
              Liên Hệ
            </button>
          </Link>
        </div>
      </div>
      <CartSidebar
        isOpen={cartSidebarOpen}
        toggleCartSidebar={toggleCartSidebar}
      />
    </header>
  );
}
