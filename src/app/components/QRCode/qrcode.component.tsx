import React from "react";
import QRCode from "react-qr-code";

function QRCodeComponent() {
  const qrValue =
    "00020101021138570010A00000072701270006970436011303710004951760208QRIBFTTA53037045802VN6304EB8A";
  return (
    <div className="mx-auto flex flex-col justify-center gap-2 rounded-lg border p-3 md:flex-row">
      <div className="relative">
        <QRCode
          value={qrValue}
          bgColor="#2b4288"
          fgColor="#fff"
          className="h-[13rem] w-[13rem] rounded-md border-b bg-vikeo-blue-900 p-3 md:h-[10rem] md:w-[10rem] md:border-b-0 md:border-r"
        />
      </div>
      <div>
        <div className="text-sm font-semibold">
          <p className="text-vikeo-pink-300">Nguyễn Thảo Vi</p>
          <p>
            STK:{" "}
            <span className="font-normal tracking-wider">0371000495176</span>
          </p>
          <p>
            Ngân hàng:{" "}
            <span className="font-normal">
              Vietcombank chi nhánh PGD Hai Bà Trưng
            </span>
          </p>
        </div>
      </div>
    </div>
  );
}

export default QRCodeComponent;
