import Link from "next/link";
import React from "react";
import { categories } from "../product";
import FooterLogo from "../Logo/footerlogo.component";

export default function RootFooter() {
  return (
    <footer className="bg-vikeo-blue-900 text-white">
      <div className="container mx-auto px-4 py-8">
        <div className="flex flex-col items-center justify-between md:flex-row md:gap-10">
          <div className="mb-6 flex flex-1 flex-col items-center gap-2 md:mb-0 md:flex-row md:gap-10">
            <div className="h-auto w-[12rem]">
              <FooterLogo />
            </div>
            <div className="mb-6 flex flex-col gap-2 text-center text-xs md:mb-0 md:gap-3 md:text-left lg:text-sm">
              <h4 className="mb-2 text-base font-bold text-vikeo-pink-300 lg:text-lg">
                Liên Hệ Vi Kẹo
              </h4>
              <p>
                <b>Địa chỉ:</b> 166/2 Đặng Văn Bi, phường Bình Thọ, Thủ Đức,
                thành phố Hồ Chí Minh.
              </p>
              <div>
                <b>Điện thoại:</b>{" "}
                <Link href="tel:0983196924"> 0983 196 924</Link>
              </div>
              <div>
                <b>Email:</b>{" "}
                <Link href="mailto:vikeobakingstudio@gmail.com">
                  vikeobakingstudio@gmail.com
                </Link>
              </div>
            </div>
          </div>

          <div className="mx-auto w-fit flex-1">
            <h4 className="mb-2 text-center text-base font-bold text-vikeo-pink-300 md:text-left">
              Danh mục
            </h4>
            <div className="mt-3 grid grid-cols-2 text-xs leading-8">
              {categories.map((cate, index) => {
                return (
                  <Link
                    key={cate + index}
                    href={`/products?tab=${index}`}
                    className="hover:underline"
                  >
                    {cate}
                  </Link>
                );
              })}
            </div>
          </div>
        </div>
      </div>
      <div className="bg-vikeo-pink-100 py-3 text-vikeo-blue-950">
        <p className="text-center text-sm md:text-base">
          &copy; 2024 Vi Kẹo Baking Studio. All rights reserved.
        </p>
      </div>
    </footer>
  );
}
