import React, { useEffect, useState } from "react";
import { categories } from "../../product";
import { Tab, TabGroup, TabList, TabPanel, TabPanels } from "@headlessui/react";
import classNames from "classnames";
import ProductCard from "../ProductCard/productcard.component";
import { Product } from "@/Types/types";
import { useSearchParams } from "next/navigation";

interface Props {
  products: Product[];
}

function ProductTab({ products }: Props) {
  const searchParams = useSearchParams();
  const tabIndex = Number(searchParams.get("tab"));
  const [selectedIndex, setSelectedIndex] = useState(0);

  useEffect(() => {
    setSelectedIndex(tabIndex);
  }, [tabIndex]);

  return (
    <TabGroup
      className="md:flex md:gap-3"
      selectedIndex={selectedIndex}
      onChange={setSelectedIndex}
    >
      <TabList className="sticky top-[5rem] mb-4 flex h-fit overflow-auto rounded-xl border border-vikeo-blue-900/30 bg-vikeo-pink-200 md:w-[20rem] md:flex-col">
        {categories.map((category) => (
          <Tab
            key={category}
            id={category}
            className={({ selected }) =>
              classNames(
                "w-full whitespace-nowrap rounded-lg px-5 py-2.5 text-sm font-medium leading-5 text-vikeo-blue-900 md:text-left",
                "focus:outline-none",
                selected
                  ? "bg-white shadow"
                  : "text-blue-100 hover:bg-white/30",
              )
            }
          >
            {category}
          </Tab>
        ))}
      </TabList>
      <TabPanels className="w-full">
        {categories.map((category) => (
          <TabPanel key={category} className="rounded-xl md:p-3 md:py-0">
            <div className="grid grid-cols-2 gap-3 sm:grid-cols-3 md:gap-8 lg:grid-cols-4">
              {products
                .filter((product) =>
                  category === "All"
                    ? true
                    : product.categories.includes(category),
                )
                .map((product) => (
                  <React.Fragment key={product.name}>
                    <ProductCard
                      product={product}
                      setSelectedIndex={setSelectedIndex}
                    />
                  </React.Fragment>
                ))}
            </div>
          </TabPanel>
        ))}
      </TabPanels>
    </TabGroup>
  );
}

export default ProductTab;
