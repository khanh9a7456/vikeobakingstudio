"use client";
import React, { ChangeEvent, useState } from "react";
import { products } from "../product";
import { ProductTab } from "./ProductTab";
import { Product } from "@/Types/types";
import { FaSearch } from "react-icons/fa";
import { removeDiacritics } from "@/utils/removeDiacritics";

function ProductsPage() {
  const [filterProducts, setFilterProducts] = useState<Product[]>(products);

  const onRenderColNameOptions = () => {
    return products?.map((p) => {
      return (
        <option key={p.name + p.price} value={p.name}>
          {p.name}
        </option>
      );
    });
  };
  return (
    <main className="min-h-[calc(100vh-138.3px)]">
      <section id="products" className="py-12">
        <h2 className="mb-8 text-center text-3xl font-bold">Sản phẩm </h2>
        <div className="px-2 md:px-4">
          <div className="flex w-full justify-end">
            <div className="relative w-full md:w-1/5">
              <input
                autoFocus
                className="my-5 w-full border border-vikeo-blue-900/40 bg-vikeo-pink-100 p-2 pl-9"
                type="text"
                placeholder="Search..."
                style={{
                  borderRadius: "0.375rem",
                  boxShadow: "none",
                }}
                onChange={(e: ChangeEvent<HTMLInputElement>) => {
                  setFilterProducts(
                    products.filter((p) =>
                      removeDiacritics(p.name).includes(
                        removeDiacritics(e.target.value),
                      ),
                    ),
                  );
                }}
                list="searchColName"
                autoComplete="off"
              />
              <datalist id="searchColName">{onRenderColNameOptions()}</datalist>
              <FaSearch className="absolute left-3 top-1/2 -translate-y-1/2" />
            </div>
          </div>
          <ProductTab products={filterProducts} />
        </div>
      </section>
    </main>
  );
}

export default ProductsPage;
