import { Product } from "@/Types/types";
import { addToCart } from "@/lib/features/cart/cartSlice";
import { useRouter } from "next/navigation";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import ImageGallery from "react-image-gallery";
import "react-image-gallery/styles/css/image-gallery.css";
import { FaCartPlus } from "react-icons/fa6";
import { categories } from "../../product";

interface Props {
  product: Product | undefined;
}

function DetailedProduct({ product }: Props) {
  const [newQuantity, setNewQuantity] = useState(1);
  const router = useRouter();
  const dispatch = useDispatch();

  if (!product) {
    return <p className="text-center">No Information</p>;
  }
  const handleAddToCart = () => {
    dispatch(addToCart({ ...product, quantity: newQuantity }));
    router.push("/cart-checkout");
  };

  const handleQuantityChange = (id: string, quantity: number) => {
    setNewQuantity(quantity);
  };

  const images = new Array(10).fill({
    original: product.image,
    thumbnail: product.image,
  });

  return (
    <div className="flex w-full flex-col gap-5 px-4 py-10 md:flex-row">
      <div className="w-full md:w-1/3">
        <ImageGallery
          items={images}
          showPlayButton={false}
          autoPlay
          showNav={false}
          lazyLoad
        />
      </div>
      <div className="mt-4 rounded-lg bg-white p-5 md:mt-0 lg:ml-8 lg:w-full">
        <h1 className="mb-4 text-3xl font-bold">{product.name}</h1>
        <p className="mb-4 text-gray-700">{product.description}</p>
        <p className="mb-4 hidden text-xl font-semibold text-gray-600 md:block">
          {product.price.toLocaleString("vi-VN", {
            style: "currency",
            currency: "VND",
            maximumFractionDigits: 0,
          })}
        </p>
        <div className="flex items-center gap-3">
          <input
            type="number"
            min="1"
            defaultValue={1}
            onChange={(e) =>
              handleQuantityChange(product.id, parseInt(e.target.value))
            }
            className="w-14 rounded-md border border-gray-300 p-2"
          />
          <button
            onClick={handleAddToCart}
            className="flex h-fit items-center justify-center gap-3 rounded-md bg-vikeo-blue-800 px-5 py-2 text-white transition-opacity hover:bg-vikeo-blue-600"
          >
            <p className="hidden md:block">Thêm vào giỏ hàng</p>
            <p className="block text-xs font-semibold text-white md:hidden md:text-sm">
              {product.price.toLocaleString("vi-VN", {
                style: "currency",
                currency: "VND",
                maximumFractionDigits: 0,
              })}
            </p>
            <FaCartPlus className="mr-3" />
          </button>
        </div>
        <div className="mt-5">
          <h3 className="py-3 text-xl font-bold">TAGS:</h3>
          <div className="flex w-full flex-wrap gap-2 text-[10px] md:text-xs">
            {product.categories.map((c, i) => {
              return (
                <div
                  key={c + 1}
                  className="cursor-pointer truncate rounded-full bg-vikeo-blue-800 px-2 py-[2px] text-white"
                  onClick={() => {
                    const cateIndex = categories.findIndex(
                      (cate) => cate === c,
                    );
                    router.push(`/products?tab=${cateIndex}`);
                  }}
                  title={c}
                >
                  {c}
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}

export default DetailedProduct;
