import { Product } from "@/Types/types";
import Image from "next/image";
import Link from "next/link";
import React, { Dispatch, SetStateAction } from "react";
import { FaCartPlus } from "react-icons/fa6";
import { categories } from "../../product";
import { useRouter } from "next/navigation";
import { useDispatch } from "react-redux";
import { addToCart } from "@/lib/features/cart/cartSlice";

interface Props {
  product: Product;
  setSelectedIndex?: Dispatch<SetStateAction<number>>;
}

function ProductCard({ product, setSelectedIndex }: Props) {
  const router = useRouter();
  const dispatch = useDispatch();

  if (!product) {
    return <p className="text-center">No Information</p>;
  }
  const handleAddToCart = () => {
    dispatch(addToCart({ ...product, quantity: 1 }));
  };
  return (
    <div className="flex flex-col justify-between overflow-hidden rounded-md border border-vikeo-blue-800 bg-white shadow-none transition-shadow hover:border-transparent hover:shadow-lg hover:shadow-vikeo-blue-500">
      <Link
        key={product.id}
        href={`/products/${product.id}`}
        className="flex-1"
      >
        <Image
          src={product.image}
          alt={product.name}
          className="mb-4 aspect-square w-full rounded-md object-cover md:h-48"
          width={50000}
          height={50000}
        />
      </Link>
      <div className="flex flex-1 flex-col justify-between">
        <div className="p-2 md:p-4">
          <div className="flex w-full flex-wrap gap-2 text-[10px] md:text-xs">
            {product.categories.map((c, i) => {
              return (
                <div
                  key={c + 1}
                  className="cursor-pointer truncate rounded-full bg-vikeo-blue-800 px-2 py-[2px] text-white"
                  onClick={() => {
                    const cateIndex = categories.findIndex(
                      (cate) => cate === c,
                    );
                    if (setSelectedIndex) {
                      setSelectedIndex(cateIndex);
                    } else router.push(`/products?tab=${cateIndex}`);
                  }}
                  title={c}
                >
                  {c}
                </div>
              );
            })}
          </div>
          <Link key={product.id} href={`/products/${product.id}`}>
            <h3
              className="mt-2 truncate whitespace-nowrap text-sm font-bold md:text-lg"
              title={product.name}
            >
              {product.name}
            </h3>
          </Link>
          <p className="hidden text-xs font-semibold text-gray-600 md:block md:text-sm">
            {product.price.toLocaleString("vi-VN", {
              style: "currency",
              currency: "VND",
              maximumFractionDigits: 0,
            })}
          </p>
          <p className="truncate text-xs font-medium text-gray-500 md:text-sm">
            {product.description}
          </p>
        </div>
        <button
          onClick={handleAddToCart}
          className="mt-3 flex w-full items-center justify-center gap-3 rounded-t-md bg-vikeo-blue-800 py-2 text-white transition-opacity hover:opacity-80"
        >
          <p className="hidden md:block">Thêm vào giỏ hàng</p>
          <p className="block text-xs font-semibold text-white md:hidden md:text-sm">
            {product.price.toLocaleString("vi-VN", {
              style: "currency",
              currency: "VND",
              maximumFractionDigits: 0,
            })}
          </p>
          <FaCartPlus className="mr-3" />
        </button>
      </div>
    </div>
  );
}

export default ProductCard;
