import Link from "next/link";

function ConfirmationPage() {
  return (
    <main className="container mx-auto px-4 py-8 text-center">
      <h1 className="mb-8 text-3xl font-bold">
        Cảm ơn bạn đã lựa chọn và mua sắm tại Vi Kẹo!
      </h1>
      <p>
        Đơn hàng của bạn đã được đặt. Chúng mình sẽ sớm liên hệ lại với bạn.
      </p>
      <p className="mt-4">
        <strong>Lưu ý:</strong> Trong trường hợp bạn chưa nhận cuộc gọi xác nhận
        từ shop, bạn vui lòng liên hệ với chúng mình qua{" "}
        <strong className="text-lg text-vikeo-blue-600">hotline:</strong>{" "}
        <Link
          href="tel:0983196924"
          className="text-lg font-semibold text-vikeo-blue-600"
        >
          {" "}
          0983 196 924
        </Link>{" "}
        hoặc fanpage:
        <Link
          href="https://www.facebook.com/vikeohomebakery/"
          target="_blank"
          className="text-lg font-semibold text-vikeo-blue-600 hover:underline"
        >
          {" "}
          Vi Kẹo Home Bakery
        </Link>
        .
      </p>
    </main>
  );
}

export default ConfirmationPage;
