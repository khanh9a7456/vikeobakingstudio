"use client";
import NextNProgress from "nextjs-progressbar";

const NextProgressClient = () => {
  return <NextNProgress />;
};

export default NextProgressClient;
