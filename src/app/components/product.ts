import { Product } from "@/Types/types";

const BASE_IMAGE_URL = "/Products";

const products: Product[] = [
  {
    id: "1",
    name: "Bánh Tiêu Nhân Phô Mai Trứng Chảy",
    price: 35000,
    image: `https://food-cms.grab.com/compressed_webp/items/VNITE2024053112362164210/detail/menueditor_item_407aadb04eb54bdd97f3bb05b4f88597_1717158944386531263.webp`,
    categories: ["Bánh Mì", "Món Ngon Theo Mùa"],
    description: "Hộp 3 bánh",
    quantity: 0,
  },
  {
    id: "2",
    name: "Bánh mì thịt nướng",
    price: 45000,
    image: `https://food-cms.grab.com/compressed_webp/items/VNITE2024051801283799134/detail/menueditor_item_48f34c9771e54e2eb82e92a63b42f74a_1715995683530286909.webp`,
    categories: ["Bánh Mì"],
    description: "Ngon",
    quantity: 0,
  },
  {
    id: "3",
    name: "Pizza Tôm Phô Mai",
    price: 49000,
    image: `https://food-cms.grab.com/compressed_webp/items/VNITE2024051206200104048/detail/menueditor_item_9759d9c59fe143cabd94e6f1c34505aa_1715494730898467426.webp`,
    categories: ["Bánh Mì"],
    description:
      "Bánh Pizza phiên bản nhanh gọn nhưng cực ngon - mustry nha mọi người 👏🏻",
    quantity: 0,
  },
  {
    id: "4",
    name: "Kem Sữa Mềm - Milky Bun",
    price: 45000,
    image: `https://food-cms.grab.com/compressed_webp/items/VNITE2024051100191945182/photo/menueditor_item_cd7f559e4fdb47ccac6ad803e5ac91aa_1715386682585250896.webp`,
    categories: ["Bánh Mì"],
    description: "Ngon",
    quantity: 0,
  },
  {
    id: "5",
    name: "Bánh Mì Phô Mai Tan Chảy size S",
    price: 25000,
    image: `https://food-cms.grab.com/compressed_webp/items/VNITE2022111805273108677/photo/menueditor_item_ff1d0d5f464344f3b1596d10a408593b_1678160167720436404.webp`,
    categories: ["Bánh Mì"],
    description:
      "Vỏ bánh mềm thơm cùng lớp bơ chà bông, nhân phô mai tan chảy. Ăn 1 bánh sẽ ăn bánh thứ 2!",
    quantity: 0,
  },
  {
    id: "6",
    name: "Bánh Mì Phô Mai Tan Chảy size M",
    price: 50000,
    image: `https://food-cms.grab.com/compressed_webp/items/VNITE2024032205353026286/detail/menueditor_item_f7cb4980919849fcbedf72a1afe0fc4e_1711085691075701848.webp`,
    categories: ["Bánh Mì"],
    description:
      "Vỏ bánh mềm thơm cùng lớp bơ chà bông, nhân phô mai tan chảy. Ăn 1 bánh sẽ ăn bánh thứ 2!",
    quantity: 0,
  },
  {
    id: "7",
    name: "Bánh Mì Nhân Kem Lạnh ( Set 3 bánh)",
    price: 45000,
    image: `https://food-cms.grab.com/compressed_webp/items/VNITE2023102812412142859/detail/menueditor_item_1a7c2277f5694e6db26412a0e7d9b017_1698496985188528622.webp`,
    categories: ["Bánh Mì"],
    description:
      "Vị socola, phô mai, matcha, truyền thống - set 3 bánh vị ngẫu nhiên. khách có nhu cầu mix vị theo ý thích vui lòng ghi chú kèm theo đơn hàng, shop sẽ liên hệ lại nếu đủ bánh",
    quantity: 0,
  },
  {
    id: "8",
    name: "Bánh Mì Nhân Kem Lạnh ( Set 4 bánh)",
    price: 60000,
    image: `https://food-cms.grab.com/compressed_webp/items/VNITE2023102612170837229/detail/menueditor_item_e3a4e6fbca5a415f82f36966edbec932_1698322575493383837.webp`,
    categories: ["Bánh Mì"],
    description: "Mix 4 nhân: kem trứng, kem socola, kem matcha, kem cheese",
    quantity: 0,
  },
  {
    id: "9",
    name: "Donut Socola ( Set 2 Bánh)",
    price: 35000,
    image: `https://food-cms.grab.com/compressed_webp/items/VNITE2022111805245260254/detail/menueditor_item_c5bef0ddb8f3489bb0c81986e104194a_1701941889989858966.webp`,
    categories: ["Bánh Mì"],
    description:
      "1 hộp 2 bánh donut mẫu ngẫu nhiên thay đổi theo ngày. bánh đc phủ socola đen và socola màu, ít ngọt cùng topping cốm màu, hạnh nhân, bánh quy,… khách lựa mẫu vui lòng ghi chú, tuỳ thuộc vào tình trạng bánh đang có ở cửa hàng, shop sẽ cố gắng phục vụ theo đúng yêu cầu.",
    quantity: 0,
  },
  {
    id: "10",
    name: "Cua Phô Mai",
    price: 20000,
    image: `https://food-cms.grab.com/compressed_webp/items/VNITE20221117065032077137/detail/menueditor_item_31176a6d525c405794b094792ca16999_1683206060442713123.webp`,
    categories: ["Bánh Mì"],
    description: "Ngon",
    quantity: 0,
  },
  {
    id: "11",
    name: "Cranberry Creamcheese ( Set 2 cái)",
    price: 30000,
    image: `https://food-cms.grab.com/compressed_webp/items/VNITE20221117065032100587/detail/menueditor_item_c7790750bfb64e6fbf3c4ebb9dec950c_1685684925177692991.webp`,
    categories: ["Bánh Mì"],
    description:
      "Bánh mềm ngọt với nhân cream cheese thêm trái berry còn gì bằng !",
    quantity: 0,
  },
  {
    id: "12",
    name: "Paparoti Mochi",
    price: 25000,
    image: `https://food-cms.grab.com/compressed_webp/items/VNITE20221117065032013208/detail/menueditor_item_d759c047e51a4cb98990a02482c02408_1685684774642063550.webp`,
    categories: ["Bánh Mì"],
    description:
      "Vỏ bánh mềm với hương cà phê, nhân bánh Mochi béo thơm mịn. Ăn 1 cái sẽ muốn ăn thêm cái nữa!",
    quantity: 0,
  },
  {
    id: "13",
    name: "Bánh mì bơ tỏi phô mai Hàn Quốc",
    price: 30000,
    image: `https://food-cms.grab.com/compressed_webp/items/VNITE20221117065032202437/detail/menueditor_item_bf4f20b8133c4d5b93e55f6c5867e1ad_1683769208382029862.webp`,
    categories: ["Bánh Mì"],
    description: "Ngon",
    quantity: 0,
  },
  {
    id: "14",
    name: "Bánh Mì Mochi Trứng Muối",
    price: 25000,
    image: `https://food-cms.grab.com/compressed_webp/items/VNITE20221117065031168833/detail/menueditor_item_e3cec1fd7da5463c833abeba3ec7665b_1683205978807560794.webp`,
    categories: ["Bánh Mì"],
    description:
      "Vỏ bánh mềm thơm bơ, phủ chà bông gà mặn mặn, nhân mochi dẻo và trứng muối !",
    quantity: 0,
  },
  {
    id: "15",
    name: "Brownies Mix",
    price: 70000,
    image: `https://food-cms.grab.com/compressed_webp/items/VNITE2024052009184885060/detail/menueditor_item_54b5a352d99c4dc8a9a07cb4208b30a4_1716196725509162645.webp`,
    categories: ["Bánh Lạnh"],
    description:
      "Bánh ngon nhất khi ăn lạnh! bạn bảo quản lạnh ít nhất 15 phút trước khi ăn nhé ❤️",
    quantity: 0,
  },
  {
    id: "16",
    name: "Tiramisu truyền thống",
    price: 50000,
    image: `https://food-cms.grab.com/compressed_webp/items/VNITE20221117065031236388/photo/menueditor_item_dd8a9dd8fe0e4689b2d5fb1ca42e9b72_1683206289251752192.webp`,
    categories: ["Bánh Lạnh"],
    description:
      "Bánh ngon nhất khi ăn lạnh! bạn bảo quản lạnh ít nhất 15 phút trước khi ăn nhé ❤️",
    quantity: 0,
  },
  {
    id: "17",
    name: "Tiramisu xoài chanh dây",
    price: 50000,
    image: `https://food-cms.grab.com/compressed_webp/items/VNITE2024051411160248636/detail/menueditor_item_dbd070220fd640e282f8b01d85ec2b43_1715685304537589550.webp`,
    categories: ["Bánh Lạnh", "Món Ngon Theo Mùa"],
    description:
      "Bánh ngon nhất khi ăn lạnh! bạn bảo quản lạnh ít nhất 15 phút trước khi ăn nhé ❤️",
    quantity: 0,
  },
  {
    id: "18",
    name: "Tiramisu dâu tây",
    price: 50000,
    image: `https://food-cms.grab.com/compressed_webp/items/VNITE2024051411144080646/detail/menueditor_item_4021d48c669d4ffcb5ceffb5faf11a5d_1715685255028646395.webp`,
    categories: ["Bánh Lạnh", "Món Ngon Theo Mùa"],
    description:
      "Bánh ngon nhất khi ăn lạnh! bạn bảo quản lạnh ít nhất 15 phút trước khi ăn nhé ❤️",
    quantity: 0,
  },
  {
    id: "19",
    name: "Panna Cotta Dâu Tây",
    price: 25000,
    image:
      "https://food-cms.grab.com/compressed_webp/items/VNITE2023081209045191830/detail/menueditor_item_e20f4c5216d444da84f288e80d2a6de7_1708164421270255686.webp",
    categories: ["Bánh Lạnh"],
    description:
      "Bánh ngon nhất khi ăn lạnh! bạn bảo quản lạnh ít nhất 15 phút trước khi ăn nhé ❤️",
    quantity: 0,
  },
  {
    id: "20",
    name: "Panna Cotta Xoài Chanh Dây",
    price: 25000,
    image:
      "https://food-cms.grab.com/compressed_webp/items/VNITE2023081209045191830/detail/menueditor_item_e20f4c5216d444da84f288e80d2a6de7_1708164421270255686.webp",
    categories: ["Bánh Lạnh"],
    description:
      "Bánh ngon nhất khi ăn lạnh! bạn bảo quản lạnh ít nhất 15 phút trước khi ăn nhé ❤️",
    quantity: 0,
  },
  {
    id: "21",
    name: "Panna Cotta Kiwi",
    price: 25000,
    image:
      "https://food-cms.grab.com/compressed_webp/items/VNITE2023081209045191830/detail/menueditor_item_e20f4c5216d444da84f288e80d2a6de7_1708164421270255686.webp",
    categories: ["Bánh Lạnh"],
    description:
      "Bánh ngon nhất khi ăn lạnh! bạn bảo quản lạnh ít nhất 15 phút trước khi ăn nhé ❤️",
    quantity: 0,
  },
  {
    id: "22",
    name: "Panna Cotta Việt Quốc",
    price: 25000,
    image:
      "https://food-cms.grab.com/compressed_webp/items/VNITE2023020408593875140/detail/menueditor_item_0d561a343ce748399783a3f50f98bace_1710647330335165050.webp",
    categories: ["Bánh Lạnh"],
    description:
      "Bánh ngon nhất khi ăn lạnh! bạn bảo quản lạnh ít nhất 15 phút trước khi ăn nhé ❤️",
    quantity: 0,
  },
  {
    id: "23",
    name: "Bánh Su Kem Mini",
    price: 35000,
    image:
      "https://food-cms.grab.com/compressed_webp/items/VNITE2023070407224523315/detail/menueditor_item_f633e31746fb4451aafe480d3c858dcd_1688454991060604446.webp",
    categories: ["Bánh Lạnh"],
    description:
      "Bánh ngon nhất khi ăn lạnh! bạn bảo quản lạnh ít nhất 15 phút trước khi ăn nhé ❤️",
    quantity: 0,
  },
  {
    id: "24",
    name: "Bánh Su Singapore",
    price: 35000,
    image:
      "https://food-cms.grab.com/compressed_webp/items/VNITE20221117065032321557/detail/menueditor_item_be1dad5f56394b4f85545539d8801ebe_1683207188017376018.webp",
    categories: ["Bánh Lạnh"],
    description:
      "Bánh ngon nhất khi ăn lạnh! bạn bảo quản lạnh ít nhất 15 phút trước khi ăn nhé ❤️",
    quantity: 0,
  },
  {
    id: "25",
    name: "Bánh bông lan trứng muối mix 2 sốt",
    price: 50000,
    image:
      "https://food-cms.grab.com/compressed_webp/items/VNITE20221117065031266865/detail/menueditor_item_28e23b438c9e4bd28c7445ae8e422756_1709523564308744423.webp",
    categories: ["Cake"],
    description:
      "Bánh ngon nhất khi ăn lạnh! bạn bảo quản lạnh ít nhất 15 phút trước khi ăn nhé ❤️",
    quantity: 0,
  },
  {
    id: "26",
    name: "Bánh bông lan trứng muối sốt phô mai",
    price: 50000,
    image:
      "https://food-cms.grab.com/compressed_webp/items/VNITE2023102809272886010/detail/menueditor_item_79a205fd347e405ba5d37dc1cc49ae6d_1698485237506585060.webp",
    categories: ["Cake"],
    description:
      "Bánh ngon nhất khi ăn lạnh! bạn bảo quản lạnh ít nhất 15 phút trước khi ăn nhé ❤️",
    quantity: 0,
  },
  {
    id: "27",
    name: "Bánh bông lan trứng muối sốt bơ mặn",
    price: 50000,
    image:
      "https://food-cms.grab.com/compressed_webp/items/VNITE2023102809272886010/detail/menueditor_item_79a205fd347e405ba5d37dc1cc49ae6d_1698485237506585060.webp",
    categories: ["Cake"],
    description:
      "Bánh ngon nhất khi ăn lạnh! bạn bảo quản lạnh ít nhất 15 phút trước khi ăn nhé ❤️",
    quantity: 0,
  },
  {
    id: "28",
    name: "Bánh kim đỉnh phô mai tươi",
    price: 50000,
    image:
      "https://food-cms.grab.com/compressed_webp/items/VNITE2022121004550434023/detail/menueditor_item_da930949c81940c9a7188046ffa87fec_1670647979370454444.webp",
    categories: ["Cake"],
    description:
      "Bánh ngon nhất khi ăn lạnh! bạn bảo quản lạnh ít nhất 15 phút trước khi ăn nhé ❤️",
    quantity: 0,
  },
  {
    id: "29",
    name: "Mochi Chấm Kem Sữa Vị Truyền Thống (Đậu Nành)",
    price: 55000,
    image:
      "https://food-cms.grab.com/compressed_webp/items/VNITE2023032005435711198/detail/menueditor_item_f48269f57276408d94acd96e7e367b2b_1679292359510795587.webp",
    categories: ["Mochi Chấm Kem Sữa"],
    description:
      "Bánh ngon nhất khi ăn lạnh! bạn bảo quản lạnh ít nhất 15 phút trước khi ăn nhé ❤️",
    quantity: 0,
  },
  {
    id: "30",
    name: "Mochi Chấm Kem Sữa Vị Socola",
    price: 55000,
    image:
      "https://food-cms.grab.com/compressed_webp/items/VNITE2023032005432538107/detail/menueditor_item_84b69a73215f4cd0accc34ff4da1203e_1679292378031636141.webp",
    categories: ["Mochi Chấm Kem Sữa"],
    description:
      "Bánh ngon nhất khi ăn lạnh! bạn bảo quản lạnh ít nhất 15 phút trước khi ăn nhé ❤️",
    quantity: 0,
  },
  {
    id: "31",
    name: "Mochi Chấm Kem Sữa Mix 3 Vị",
    price: 70000,
    image:
      "https://food-cms.grab.com/compressed_webp/items/VNITE2023032003430044272/detail/menueditor_item_703e1b50c2da441d8eeeb16f9c514fed_1683206826304294806.webp",
    categories: ["Mochi Chấm Kem Sữa"],
    description:
      "Bánh ngon nhất khi ăn lạnh! bạn bảo quản lạnh ít nhất 15 phút trước khi ăn nhé ❤️",
    quantity: 0,
  },
  {
    id: "32",
    name: "Mochi Chấm Kem Sữa Vị Matcha",
    price: 55000,
    image:
      "https://food-cms.grab.com/compressed_webp/items/VNITE2023032005442092912/detail/menueditor_item_c1cd5eb581434849bbf256087088d464_1679292333919392932.webp",
    categories: ["Mochi Chấm Kem Sữa"],
    description:
      "Bánh ngon nhất khi ăn lạnh! bạn bảo quản lạnh ít nhất 15 phút trước khi ăn nhé ❤️",
    quantity: 0,
  },
  {
    id: "33",
    name: "Bánh bò thốt nốt nướng",
    price: 45000,
    image:
      "https://food-cms.grab.com/compressed_webp/items/VNITE2023072207260580006/detail/menueditor_item_b83269d0565e4dc09281a848e036a6e3_1690010728724104479.webp",
    categories: ["Others"],
    description:
      "Bánh ngon nhất khi ăn lạnh! bạn bảo quản lạnh ít nhất 15 phút trước khi ăn nhé ❤️",
    quantity: 0,
  },
  {
    id: "34",
    name: "Bánh bò mix khoai mì dừa",
    price: 55000,
    image:
      "https://food-cms.grab.com/compressed_webp/items/VNITE2023072207250684105/detail/menueditor_item_2171e1ccae97478d8c73bcae89f20814_1690012007838087147.webp",
    categories: ["Others"],
    description:
      "Bánh ngon nhất khi ăn lạnh! bạn bảo quản lạnh ít nhất 15 phút trước khi ăn nhé ❤️",
    quantity: 0,
  },
  {
    id: "35",
    name: "Khoai mì nhân dừa non",
    price: 50000,
    image:
      "https://food-cms.grab.com/compressed_webp/items/VNITE2023072207274497169/detail/menueditor_item_5e1d92b03d70438999e9003fee245940_1690012061211818874.webp",
    categories: ["Others"],
    description:
      "Bánh ngon nhất khi ăn lạnh! bạn bảo quản lạnh ít nhất 15 phút trước khi ăn nhé ❤️",
    quantity: 0,
  },
  {
    id: "36",
    name: "Sữa Chua Nếp Cẩm",
    price: 22000,
    image:
      "https://food-cms.grab.com/compressed_webp/items/VNITE2023030311310965070/detail/menueditor_item_53ad7972dcbb4475967160883d468e28_1677843004260411672.webp",
    categories: ["Ăn vặt zui zui"],
    description:
      "100% nhà làm, công thức siêu thơm béo cùng nếp cẩm nấu chín mềm, nhai sực sực siêu ngon lun ạ",
    quantity: 0,
  },
  {
    id: "37",
    name: "Sữa Chua uống trái cây",
    price: 28000,
    image:
      "https://food-cms.grab.com/compressed_webp/items/VNITE2023110209362310512/detail/menueditor_item_72254c648e704fcabe0e3f570ed54f69_1698917721213517354.webp",
    categories: ["Ăn vặt zui zui"],
    description:
      "100% nhà làm, công thức siêu thơm béo cùng nếp cẩm nấu chín mềm, nhai sực sực siêu ngon lun ạ",
    quantity: 0,
  },
  {
    id: "38",
    name: "Trà sữa oolong đài loan",
    price: 35000,
    image:
      "https://food-cms.grab.com/compressed_webp/items/VNITE2024050205085305468/detail/menueditor_item_9f444411a4b34a768218a4ad5c47a9ef_1714626518503582152.webp",
    categories: ["Ăn vặt zui zui"],
    description:
      "100% nhà làm, công thức siêu thơm béo cùng nếp cẩm nấu chín mềm, nhai sực sực siêu ngon lun ạ",
    quantity: 0,
  },
  {
    id: "39",
    name: "Trà dâu tây tươi",
    price: 35000,
    image:
      "https://food-cms.grab.com/compressed_webp/items/VNITE2024031606341695641/detail/menueditor_item_3c99582f14e2433c88785ce70045d6c6_1710570766749253334.webp",
    categories: ["Ăn vặt zui zui"],
    description:
      "100% nhà làm, công thức siêu thơm béo cùng nếp cẩm nấu chín mềm, nhai sực sực siêu ngon lun ạ",
    quantity: 0,
  },
];
const categories = [
  "All",
  "Món Ngon Theo Mùa",
  "Bánh Mì",
  "Bánh Lạnh",
  "Cake",
  "Mochi Chấm Kem Sữa",
  "Ăn vặt zui zui",
  "Others",
];

export { products, categories };
