import Image from "next/image";
import React from "react";
import logoBlue from "/public/Logo/name-blue.png";
import Link from "next/link";

function Logo() {
  return (
    <Link href="/" className="h-full w-28 p-1">
      <Image
        src={logoBlue}
        alt="Logo Blue"
        className="h-auto w-screen object-cover saturate-100"
        width={90000000}
        height={90000000}
      />
    </Link>
  );
}

export default Logo;
