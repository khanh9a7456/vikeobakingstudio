import React from "react";
import { ConfirmationPage } from "../components/Confirmation";

function Confirmation() {
  return <ConfirmationPage />;
}

export default Confirmation;
