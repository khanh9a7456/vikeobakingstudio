import React from "react";
import { ProductsPage } from "../components/Products";

function Products() {
  return <ProductsPage />;
}

export default Products;
