"use client";

import { Product } from "@/Types/types";
import { ProductDetails } from "@/app/components/Products/ProductDetails";
import { products } from "@/app/components/product";
import { useParams } from "next/navigation";
import React from "react";

function getData() {
  const res = products;

  if (!res) {
    throw new Error("Failed to fetch data");
  }

  return res as unknown as Product[];
}

function DetailedProduct() {
  const { productId } = useParams();
  const data = getData();

  if (!data) {
    return <></>;
  }

  return (
    <ProductDetails
      product={data?.find((item) => Number(item.id) === Number(productId))}
    />
  );
}

export default DetailedProduct;
