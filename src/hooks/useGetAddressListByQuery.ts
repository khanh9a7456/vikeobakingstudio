import { IAddressItem } from "@/Types/types";
import { addressService } from "@/services/addressService";
import { useQuery } from "@tanstack/react-query";

export const useGetAddressListByQuery = (query: string) => {
  return useQuery<IAddressItem[]>({
    queryKey: ["getAddressListByQuery", query],
    queryFn: async () => await addressService.getAddressData(query),
  });
};
