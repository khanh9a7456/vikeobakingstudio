import { IAddressItem } from "@/Types/types";
import { addressService } from "@/services/addressService";
import { useQuery } from "@tanstack/react-query";

export const useGetGrabAddressListByQuery = (query: string) => {
  return useQuery<IAddressItem[]>({
    queryKey: ["getGrabAddressListByQuery", query],
    queryFn: async () => await addressService.getGrabAddressData(query),
  });
};
