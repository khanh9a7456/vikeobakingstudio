import { IShippingInfo } from "@/Types/types";

export const postCheckoutData = async (data: IShippingInfo) => {
  const formCheckoutData = new FormData();
  formCheckoutData.append("entry.796979262", data.name);
  formCheckoutData.append("entry.1301863036", data.phone);
  formCheckoutData.append("entry.2098646374", data.address);
  formCheckoutData.append(
    "entry.1929795080",
    data.deliveryType === "get" ? "Nhận tại quán" : "Shop đặt ship",
  );
  formCheckoutData.append(
    "entry.12661304",
    data.orderType === "order-now" ? "Đặt ngay" : "Đặt trước",
  );
  formCheckoutData.append("entry.1105438289", data["receive-time"]);
  formCheckoutData.append("entry.614160035", data.products);
  formCheckoutData.append(
    "entry.2135971256",
    data.paymentMethod === "cod" ? "COD" : "Chuyển khoản",
  );
  formCheckoutData.append("entry.54575644", data.total);

  fetch(
    "https://docs.google.com/forms/d/e/1FAIpQLSdOornMj4bFj3FelN-ZXx3XxODTWzIYSsJAWCUH55zVtv37AA/formResponse",
    {
      method: "POST",
      body: formCheckoutData,
      mode: "no-cors",
    },
  );
};
