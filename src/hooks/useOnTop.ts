import { useState, useEffect } from "react";

const useOnTop = (): boolean => {
  const [isOnTop, setIsOnTop] = useState<boolean>(true);

  const checkScrollTop = (): void => {
    setIsOnTop(window.scrollY === 0);
  };

  useEffect(() => {
    // Check scroll position when the component mounts
    checkScrollTop();

    // Add scroll event listener
    window.addEventListener("scroll", checkScrollTop);

    // Clean up event listener on unmount
    return () => {
      window.removeEventListener("scroll", checkScrollTop);
    };
  }, []);

  return isOnTop;
};

export default useOnTop;
