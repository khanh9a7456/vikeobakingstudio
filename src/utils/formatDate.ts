export const formatDate = (date: Date): string => {
  const daysOfWeek = [
    "Chủ Nhật",
    "Thứ Hai",
    "Thứ Ba",
    "Thứ Tư",
    "Thứ Năm",
    "Thứ Sáu",
    "Thứ Bảy",
  ];

  const dayOfWeek = daysOfWeek[date.getDay()];
  const day = date.getDate();
  const month = date.getMonth() + 1; // Months are zero-based
  const year = date.getFullYear();
  const hours = date.getHours();
  const minutes = date.getMinutes();

  const formattedDate = `Ngày đặt: ${dayOfWeek}, ngày ${day} tháng ${month} năm ${year}`;
  const formattedTime = `Thời gian: ${hours}h${minutes.toString().padStart(2, "0")}`;

  return `${formattedDate}\n${formattedTime}`;
};
