/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "lh3.googleusercontent.com",
        port: "",
        pathname: "/fife/**",
      },
      {
        protocol: "https",
        hostname: "food-cms.grab.com",
        port: "",
        pathname: "/compressed_webp/items/**",
      },
    ],
  },
};

export default nextConfig;
